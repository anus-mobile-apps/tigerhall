import * as React from 'react';
import { ChakraProvider } from "@chakra-ui/react";
import Search from './containers/Search';
import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { HttpLink } from 'apollo-link-http';
import { ApolloProvider } from '@apollo/react-hooks';

function App() {

  const cache = new InMemoryCache();
  const link = new HttpLink({
    uri: 'https://api.staging.tigerhall.io/graphql'
  })

  const client = new ApolloClient({
    cache,
    link
  })
  return (
    <ApolloProvider client={client}>
      <ChakraProvider>
        <Search />
      </ChakraProvider>
    </ApolloProvider>
  );
}

export default App;
