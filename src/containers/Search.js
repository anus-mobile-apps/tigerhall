import { useState, useEffect } from "react";
import { Box, Image, Container, Input } from "@chakra-ui/react";
import { useQuery } from '@apollo/react-hooks';
import gql from "graphql-tag";

import Loader from '../loader.svg'

const GET_PODCASTS = gql
	`
{
	contentCards(filter: { limit: 20, keywords: "", types: [PODCAST] }) {
		edges {
 ...on Podcast {
				name
				image {
 ...Image
				}
				categories {
 ...Category
				}
				experts {
 ...Expert
				}
			}
		}
	}
}
fragment Image on Image {
	uri
}
fragment Category on Category {
	name
}
fragment Expert on Expert {
	firstName
	lastName
	title
	company
}
`


function Search() {

	const [podcastList, setPodcastList] = useState([]);
	const [podcastListFilter, setPodcastListFilter] = useState([]);
	const [filter, setFilter] = useState("")
	const [loader, setLoader] = useState(false)
	const { data, loading } = useQuery(GET_PODCASTS);

	useEffect(() => {

		if (data?.contentCards?.edges) {

			setPodcastList(data.contentCards.edges);
			setPodcastListFilter(data.contentCards.edges);

		}

	}, [data])

	useEffect(() => {

		setLoader(true)

		setTimeout(() => {

			let podList = podcastList;

			if (filter) {
				podList = podList.filter(item => new RegExp(String(filter).toLowerCase()).test(String(item.name).toLowerCase()));
			}

			setPodcastListFilter(podList);
			setLoader(false)

		}, 300)


	}, [filter])


	function getImageUri(image) {

		let width = '250' // added static width as the resize not working width dynamic width.

		return image.replace("/uploads/", `/resize/${width}x/uploads/`)


	}

	return (

		<Container backgroundColor="#001315" alignItems="center" padding="50px" centerContent maxWidth="100%">


			<Box
				alignSelf="flex-start"
				fontSize="14px"
				color="#ffffff"
				fontWeight="700"
				lineHeight="17.3px"
				height="20px"
			>Search</Box>

			<Input placeholder="Type any keyword" backgroundColor="#003238"
				borderWidth="0px"
				color="#ffffff"
				borderRadius="5px"
				size="md" onChange={(e) => { setFilter(e.target.value) }} />

			{(loading || loader) && podcastListFilter.length === 0 ?

				<Box
					alignSelf="center"
					padding="20"
					color="#ffffff"
					height="40px">
					<Image maxWidth="100%" src={Loader} alt={"Loading..."} ignoreFallback />
				</Box> :

				podcastListFilter.map((podcast) => {
					return (

						<Box maxWidth="100%" borderWidth="0px" borderRadius="5px" overflow="hidden" marginTop="10px" box-shadow="0px -1.25455px 3.76364px rgba(0, 0, 0, 0.0941176)">


							<Image minWidth="100%" src={getImageUri(podcast.image.uri)} alt={podcast.name} ignoreFallback />

							<Box p="3.5" backgroundColor="#ffffff">

								<Box maxWidth="100%" margin="4px 0px">

									{podcast.categories && podcast.categories.map((category) =>
										<Box borderRadius="full" colorScheme="teal" color="#ff8615" fontWeight="700"
											fontSize="12px"
											textTransform="uppercase"
											lineHeight="14.83px"
										>
											{category.name}
										</Box>
									)}

									<Box
										color="#000"
										fontWeight="700"
										lineHeight="22.25px"
										letterSpacing="wide"
										fontSize="18px"
									>
										{podcast.name}
									</Box>
								</Box>

								{podcast.experts && podcast.experts.map((expert) =>
									<>
										<Box
											mt="1"
											as="h4"
											lineHeight="17.3px"
											fontWeight="600"
											color="#333333"
										>
											{expert.firstName + " " + expert.lastName}
										</Box>

										<Box
											fontWeight="600"
											color="#333333"
										>
											{expert.title}
										</Box>


										<Box color="#ff8615" fontSize="14px" lineHeight="17.3px" fontWeight="600" paddingBottom="20px">
											{expert.company}
										</Box>

									</>
								)}
							</Box>



						</Box>

					)
				})}

		</Container>
	)

}

export default Search;
